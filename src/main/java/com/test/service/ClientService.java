package com.test.service;

import com.test.domain.User;
import com.test.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    UserRepository repository;

    public List<User> getAllUsers()
    {
        List<User> userList = repository.findAll();

        if(userList.size() > 0) {
            return userList;
        } else {
            return new ArrayList<User>();
        }
    }

    public User getUserById(Long id)
    {
        Optional<User> user = repository.findById(id);

        if(user.isPresent()) {
            return user.get();
        } else {
            System.out.println("");
            return new User();
        }
    }


    public boolean deleteUserById(Long id)
    {
        Optional<User> optionalUser = repository.findById(id);
        if(optionalUser.isPresent())
        {
            repository.deleteById(id);
            return true;
        }
        return false;
    }
}
