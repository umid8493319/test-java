package com.test.service;

import com.test.domain.City;
import com.test.domain.User;
import com.test.repos.CityRepository;
import com.test.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    CityRepository repository;

    public List<City> getAll()
    {
        List<City> cityList = repository.findAll();

        if(cityList.size() > 0) {
            return cityList;
        } else {
            return new ArrayList<City>();
        }
    }

    public City getCityById(Long id)
    {
        Optional<City> city = repository.findById(id);

        if(city.isPresent()) {
            return city.get();
        } else {
            System.out.println("");
            return null;
        }
    }
    public void editWeather(City city,Long weather)
    {
        city.setCityWeather(weather);
        createOrUpdateCity(city);
    }

    public City createOrUpdateCity(City city)
    {
        Optional<City> optionalCity = repository.findById(city.getId());

        if(optionalCity.isPresent())
        {
            City newCity =  new City(optionalCity.get());
            repository.save(newCity);
            return newCity;
        } else {
            repository.save(city);
            return city;
        }
    }

    public boolean deleteUserById(Long id)
    {
        Optional<City> optionalCity = repository.findById(id);
        if(optionalCity.isPresent())
        {
            repository.deleteById(id);
            return true;
        }
        return false;
    }
}
