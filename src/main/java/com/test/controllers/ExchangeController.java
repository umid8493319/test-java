package com.test.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.domain.Exchange;
import com.test.domain.ExchangeResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
@RequestMapping("/exchange")
public class ExchangeController {

    @GetMapping("")
    public ExchangeResponse exchange() {
        String queryUrl = "https://openexchangerates.org/api/latest.json?app_id=4208f158c43c48ca9d82ffdca6f62758&base=USD&symbols=UZS,RUB";
        ExchangeResponse exchangeResponse = new ExchangeResponse();
        StringBuffer response = null;
        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) new URL(queryUrl).openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(250);
            connection.setReadTimeout(250);
            connection.setUseCaches(false);

            connection.connect();
            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                ObjectMapper objectMapper = new ObjectMapper();
                Exchange exchange = objectMapper.readValue(response.toString(), Exchange.class);
                exchangeResponse = new ExchangeResponse(exchange, true, 0);

                return exchangeResponse;
            } else {
                System.out.println("GET request not worked");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection.disconnect();
        }
        return exchangeResponse;
    }


}
