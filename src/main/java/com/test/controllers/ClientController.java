  package com.test.controllers;


  import com.test.domain.User;
  import com.test.service.UserService;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.http.HttpHeaders;
  import org.springframework.http.HttpStatus;
  import org.springframework.http.ResponseEntity;
  import org.springframework.web.bind.annotation.*;

  @RestController
  @RequestMapping("/users")
  public class ClientController {
      @Autowired
      UserService service;

      @GetMapping("/register")
      public Object getUserList(@PathVariable("id") Long id) {
          User user = service.getUserById(id);
          if (!user.isUserFound()) {
             return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
      }

      @GetMapping("/cities-list")
      public Object getUserDetails(@PathVariable("id") Long id) {
          User user = service.getUserById(id);
          if (!user.isUserFound()) {
             return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
      }

      @PostMapping("/subscribe-to-city")
      public Object editUser(@PathVariable("id") Long id) {
          User user = service.getUserById(id);
          if (!user.isUserFound()) {
             return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
      }
      @PostMapping("/get-subscriptions")
      public Object citiesList(@PathVariable("id") Long id) {
          User user = service.getUserById(id);
          if (!user.isUserFound()) {
             return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }
          return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
      }

  }
