  package com.test.controllers;


  import com.test.domain.City;
  import com.test.domain.User;
  import com.test.service.CityService;
  import com.test.service.UserService;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.http.HttpHeaders;
  import org.springframework.http.HttpStatus;
  import org.springframework.http.ResponseEntity;
  import org.springframework.web.bind.annotation.*;

  import java.util.List;

  @RestController
  @RequestMapping("/users")
  public class AdminController {
      @Autowired
      UserService userservice;
      @Autowired
      CityService cityService;


      @GetMapping("/user-list")
      public Object getUserList() {
          List<User> allUsers = userservice.getAllUsers();
          if (allUsers.isEmpty()) {
             return new ResponseEntity<String>("no users",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<>(allUsers, new HttpHeaders(), HttpStatus.OK);
      }

      @GetMapping("/user-details")
      public Object getUserDetails(@PathVariable("userId") Long userId) {
          User user = userservice.getUserById(userId);
          if (!user.isUserFound()) {
             return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<>(user, new HttpHeaders(), HttpStatus.OK);
      }

      @PostMapping("/edit-user")
      public Object editUser(@RequestBody User user) {
          User updateUser = userservice.createOrUpdateUser(user);
          return new ResponseEntity<>(updateUser, new HttpHeaders(), HttpStatus.OK);
      }

      @GetMapping("/cities-list")
      public Object citiesList() {
          List<City> all = cityService.getAll();
          if (all.isEmpty()) {
             return new ResponseEntity<String>("Cities not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
          }
          return new ResponseEntity<>(all, new HttpHeaders(), HttpStatus.OK);
      }

      @PostMapping("/edit-city")
      public Object editCity(@RequestBody() City city) {
          City updateCity = cityService.createOrUpdateCity(city);
          return new ResponseEntity<>(updateCity, new HttpHeaders(), HttpStatus.OK);
      }

      @PostMapping("/update-city-weather ")
      public Object updateCityWeather (@PathVariable("city_id") Long id,@PathVariable("weather") Long weather) {
          City city = cityService.getCityById(id);
          if (city == null)
              return new ResponseEntity<String>("no users",new HttpHeaders(), HttpStatus.NOT_FOUND);
          cityService.editWeather(city,weather);

          return new ResponseEntity<>("OK", new HttpHeaders(), HttpStatus.OK);
      }

  }
