  package com.test.controllers;


import com.test.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.test.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService service;

    @GetMapping("")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> list = service.getAllUsers();

        return new ResponseEntity<List<User>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public Object getUserById(@PathVariable("id") Long id) {
        User user = service.getUserById(id);
        if (!user.isUserFound()) {
           return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<User> createOrUpdateUser(User user) {
        User updated = service.createOrUpdateUser(user);
        return new ResponseEntity<User>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public Object deleteUserById(@PathVariable("id") Long id) {
        boolean isSuccess = service.deleteUserById(id);
        if (isSuccess)
            return new ResponseEntity<String>("User Deleted",new HttpHeaders(), HttpStatus.OK);
        else
            return new ResponseEntity<String>("User not Found",new HttpHeaders(), HttpStatus.NOT_FOUND);

    }

}
