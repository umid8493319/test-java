package com.test.domain;


import javax.persistence.*;

@Entity
@Table(name = "subscribes")
public class Subscribe {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SERVICE_NAME")
    private String serviceName;

    @Column(name = "TARGET_URL")
    private String targetUrl;

    @Column(name = "PRICE")
    private int price;

    @Column(name = "user_id")
    private String userId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public Subscribe(){}
    public Subscribe(Long id, String serviceName, String targetUrl, int price, boolean visibility) {
        this.id = id;
        this.serviceName = serviceName;
        this.targetUrl = targetUrl;
        this.price = price;
    }

    public String getId() {
        return id.toString();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Subscribe [id=" + id + ", serviceName=" + serviceName +
                ", targetUrl=" + targetUrl + ", price=" + price   + "]";
    }

}