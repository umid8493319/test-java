package com.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Exchange {

    @JsonProperty("disclaimer")
    String Date;
    @JsonProperty("license")
    String PreviousDate;
    @JsonProperty("timestamp")
    String PreviousURL;
    @JsonProperty("base")
    String base;
    @JsonProperty("rates")
    Rate rates;

    static class Rate {
        @JsonProperty("RUB")
        String rub;
        @JsonProperty("UZS")
        String uzs;
    }

}
