package com.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeResponse {

    @JsonProperty("rub_kurs")
    private String rubKurs;

    @JsonProperty("uzb_kurs")
    private String uzbKurs;

    @JsonProperty("success_code")
    private int successCode;

    @JsonProperty("success")
    private Boolean success;

    public ExchangeResponse() {
        this.success = false;
        this.successCode = -1;
        this.rubKurs = "";
        this.uzbKurs = "";
    }

    public ExchangeResponse(Exchange exchange, Boolean success, int successCode) {
        this.rubKurs = exchange.rates.rub;
        this.uzbKurs = exchange.rates.uzs;
        this.success = success;
        this.successCode = successCode;
    }


}
