package com.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name = "users")
public class City {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "city_name")
    private String cityName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "user_id")
    private String userId;


    @Column(name = "city_weather")
    private Long cityWeather;

    @Column(name = "visibility")
    private boolean visibility;

    public City() {
        this.id = 0L;
    }

    public City(City city) {
        this.cityName = city.getCityName();
        this.id = city.getId();
        this.userId = city.getUserId();
        this.cityWeather = city.getCityWeather();
    }

    public boolean isVisible() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }



    public City(Long id, String cityName, String lastName) {
        this.id = id;
        this.cityName = cityName;
    }

    public Long getId() {
        return id;
    }

    public String getId(int a) {
        return id.toString();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getCityWeather() {
        return cityWeather;
    }

    public void setCityWeather(Long cityWeather) {
        this.cityWeather = cityWeather;
    }


}
