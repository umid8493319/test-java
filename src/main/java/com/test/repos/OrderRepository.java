package com.test.repos;

import com.test.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<User,Long> {
}
